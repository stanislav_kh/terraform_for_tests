variable "profile" { default = "hobbiton" }
variable "region" { default = "eu-west-1" }
variable "environment" { default = "testing" }
variable "hostname" { default = "localhost" }
variable "empty_test_var" {}

locals {
  workspace_hash = sha1(terraform.workspace)
  workspace_name = "test-slot-${local.workspace_hash}"
}

provider "aws" {
  profile = var.profile
  region  = var.region
}

terraform {
  backend "s3" {
    bucket = "${var.profile}-${var.region}-terraform-states"
    key     = "testing/terraform-test-platform/helm_chart_test-deployer-test.tfstate"
    region = var.region
    encrypt = "true"
  }
}

data "terraform_remote_state" "test_slot" {
  backend   = "s3"
  workspace = terraform.workspace
  config = {
    bucket  = "${var.profile}-${var.region}-terraform-states"
    key     = "${var.environment}/helm_chart_terraform-test-platform.tfstate"
    region  = var.region
    profile = var.profile
    encrypt = "true"
  }
}

provider "ssh" {
  port = 25555
}

data "ssh_tunnel" "k8s" {
  host           = "bastion.${var.environment}.${var.profile}.cta.eu.amp.cisco.com"
  local_address  = "localhost:0" // use port 0 to request an ephemeral port (a random port)
  remote_address = "api.k8s.${var.environment}.${var.profile}.cta.eu.amp.cisco.com:443"
}

provider "kubernetes" {
  config_context         = "k8s.${var.environment}.${var.profile}.cta.eu.amp.cisco.com"
  config_context_cluster = "k8s.${var.environment}.${var.profile}.cta.eu.amp.cisco.com"
  host                   = "https://api.k8s.${var.environment}.${var.profile}.cta.eu.amp.cisco.com:${data.ssh_tunnel.k8s.port}"
}

provider "helm" {
  kubernetes {
    config_context = "k8s.${var.environment}.${var.profile}.cta.eu.amp.cisco.com"
    host = "https://api.k8s.${var.environment}.${var.profile}.cta.eu.amp.cisco.com:${data.ssh_tunnel.k8s.port}"
  }
}

resource "helm_release" "nginx-from-other-repo-helm" {
  name = "nginx-from-other-repo-helm"
  chart = "${path.module}/nginx-pod-other-helm"
  version = "0.2.0"
  namespace = local.workspace_name
  timeout = 3000 // 10 mins
  wait = true

  set {
    name = "empty_test_var"
    value = var.empty_test_var
  }

}

output "empty_test_var" {
  value = var.empty_test_var
}
