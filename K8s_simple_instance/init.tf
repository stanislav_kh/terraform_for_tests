variable "profile" { default = "hobbiton" }
variable "region" { default = "eu-west-1" }
variable "environment" { default = "testing" }
variable "hostname" { default = "localhost" }
variable "empty_test_var" {}

locals {
  workspace_hash = sha1(terraform.workspace)
  # vpc_id         = data.terraform_remote_state.test_slot.outputs.development_vpc_id
  workspace_name = "test-slot-${local.workspace_hash}"
}

provider "aws" {
  profile = var.profile
  region  = var.region
}

terraform {
  backend "s3" {
    bucket = "${var.profile}-${var.region}-terraform-states"
    key     = "testing/terraform-test-platform/simple_pod_test-deployer-test.tfstate"
    region = var.region
    encrypt = "true"
  }
}

data "terraform_remote_state" "test_slot" {
  backend   = "s3"
  workspace = terraform.workspace
  config = {
    bucket  = "${var.profile}-${var.region}-terraform-states"
    key     = "${var.environment}/simple_pod_terraform-test-platform.tfstate"
    region  = var.region
    profile = var.profile
    encrypt = "true"
  }
}

provider "ssh" {
  port = 5555
}

data "ssh_tunnel" "k8s" {
  host           = "bastion.${var.environment}.${var.profile}.cta.eu.amp.cisco.com"
  local_address  = "localhost:0" // use port 0 to request an ephemeral port (a random port)
  remote_address = "api.k8s.${var.environment}.${var.profile}.cta.eu.amp.cisco.com:443"
}

provider "kubernetes" {
  config_context         = "k8s.${var.environment}.${var.profile}.cta.eu.amp.cisco.com"
  config_context_cluster = "k8s.${var.environment}.${var.profile}.cta.eu.amp.cisco.com"
  host                   = "https://api.k8s.${var.environment}.${var.profile}.cta.eu.amp.cisco.com:${data.ssh_tunnel.k8s.port}"
}

resource "kubernetes_pod" "test_pod-from-other-repo" {
  metadata {
    name = "dummy-nginx-from-other-repo"
    namespace = local.workspace_name
    labels = {
      name = "K8s-test-from-other-repo"
    }
  }
  spec {
    container {
      name = "dummy-nginx-container-from-other-repo"
      image = "nginx:1.7.5"
      port {
        container_port = 80
      }
    }
  }
}

output "empty_test_var" {
  value = var.empty_test_var
}
